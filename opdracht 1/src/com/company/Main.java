package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int Gradeamount = 0;

        System.out.println( "Enter the amount of grades you have and press 'Enter' :" );
        Gradeamount = scan.nextInt();

        double Grades[] = new double[Gradeamount];

        System.out.println( "Please enter "+Gradeamount+" grades : " );

        InsertGrade( Grades );

        Print( Grades );

        Average( Grades );

        Passed( Grades );

    }

    public static void InsertGrade( double[] x) {
        Scanner scanny = new Scanner(System.in);

        for( int i = 0; i < x.length; i++) {
            System.out.println( "Enter Grade "+(i+1)+" :");
            double grade = scanny.nextDouble();

            if( Check( grade ) == true) {
                x[i] = grade;
            }
            else {
                System.out.println("Please enter a number between 1 and 10.");
                i--;
            }
        }
    }

    public static boolean Check( double x) {
        if( x >= 1 && x <= 10) {
            return true;
        }
        else{
            return false;
        }
    }

    public static void Print( double[] x) {
        System.out.println("You entered the following grades : ");

        for( int i = 0; i < x.length; i++) {
            System.out.println( "Grade "+(i+1)+": "+x[i] );
        }
    }

    public static void Average( double[] x) {
        double added = 0, average = 0;

        for( int i = 0; i < x.length; i++) {
            added += x[i];
        }

        average = added / x.length;

        System.out.println( "The average is : "+average );
    }

    public static void Passed( double[] x) {
        int higher = 0;

        System.out.println( "The following grade(s) have passed : ");

        for( int i = 0; i < x.length; i++) {
            if( x[i] >= 5.5 ){
                System.out.println( "Grade " +(i+1)+ ": " +x[i]);
                higher++;
            }
        }

        System.out.println( higher+ " out of " +x.length+ " grades have passed.");
    }
}
